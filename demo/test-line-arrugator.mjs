
import LineArrugator from "../line-arrugator.mjs";
import proj4 from "../3rd-party/proj4-esm.mjs";


// Universal Traverse Mercator, zone 30N (UTM30N)
proj4.defs("EPSG:25830","+proj=utm +zone=30 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");


let arruga = new LineArrugator(
	proj4('EPSG:4326','EPSG:25830').forward,
	[[-50, 0], [40, 25]]
);


// arruga.step();
// arruga.step();
// arruga.step();
// arruga.step();
// arruga.step();

arruga.epsilon = 1000;

// console.log(arruga);
// console.log(arruga._queue.data);

console.log(arruga.output());
